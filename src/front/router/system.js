/**
 *<p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : </li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月04日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */

export default [
  {
    path: 'system/permission',
    name: 'SystemPermission',
    component: resolve => require(['../views/system/Permission'], resolve),
    meta: {
      navText: '权限管理'
    },
    children: [
      {
        path: 'form',
        name: 'SystemPermissionForm',
        component: resolve => require(['../views/system/Permission/Form'], resolve),
        meta: {
          navText: '权限表单'
        }
      }
    ]
  },
  {
    path: 'system/role',
    name: 'SystemRole',
    component: resolve => require(['../views/system/Role'], resolve),
    meta: {
      navText: '角色管理'
    },
    children: [
      {
        path: 'form',
        name: 'SystemRoleForm',
        component: resolve => require(['../views/system/Role/Form'], resolve),
        meta: {
          navText: '角色表单'
        }
      },
      {
        path: 'grant',
        name: 'SystemRoleGrant',
        component: resolve => require(['../views/system/Role/Grant'], resolve),
        meta: {
          navText: ' 角色授权'
        }
      }
    ]
  },
  {
    path: 'system/user',
    name: 'SystemUser',
    component: resolve => require(['../views/system/User'], resolve),
    meta: {
      navText: '用户管理'
    },
    children: [
      {
        path: 'form',
        name: 'SystemUserForm',
        component: resolve => require(['../views/system/User/Form'], resolve),
        meta: {
          navText: '用户表单'
        }
      }
    ]
  }
];
